﻿## Use a MySQL docker container to run the tests on your machine.
Before running a new container, you should know that this is a lightweight MySQL server that has been developed by MySQL team.


The documentation of this image is available [here](https://hub.docker.com/_/mysql).


Using containers allows us to create and delete ephemeral database servers, so testing on new pre-configured databases is easy and fast. 


Keep in mind that containers created from this image doesn't store the data of the databases in the container itself, it stores them in the data volume
of your docker, **each time you stop the container, the data will be removed automatically with the container.**

### How to create and run a container?
1. open your console and pull the MySQL Docker image.
2. run the next command to create the container (be sure to be in project directory):

```docker run --rm -dit -e MYSQL_ROOT_PASSWORD="Password&123" -e MYSQL_USER="adm" -e MYSQL_PASSWORD="adm" -e MYSQL_ROOT_HOST=% -p 3306:3306 -v $(pwd)/sql_script.sql:/docker-entrypoint-initdb.d/init.sql --name localMysql mysql```
> To stop the container, you can use the next command:
>```docker stop localMysql```



## Testing strategy

### Unit and Integration testing
* We went with Mockito & JUnit :
    * JUnit : integration tests on ActionsBDImpl class with MySQL container described above and a unit test on ProgrammeurBean class.
    * Mockito : unit testing on ActionsBDImpl, using mock and spy objects. Faked connection with database.

* GitLab CI :
    * Pipeline designed with 2 stages : smoke testing & unit/integration testing with Maven and MySQL images (described in the YML file).
    * Reports generated from test results to explain which tests failed and why.

* Datasets:
    * Extensive dataset with several user profiles, to try and test a widespread of use cases, with expected outcomes as well.