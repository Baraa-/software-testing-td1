package util;

import modele.ActionsBD;
import modele.ProgrammeurBean;

import java.sql.Date;
import java.time.LocalDate;

public class ProgrammeurBeanHelper {
    public ProgrammeurBean of(String matricule,
                              String nom,
                              String prenom,
                              String secondPrenom,
                              String adresse,
                              String pseudo,
                              String responsable,
                              String hobby,
                              Date date_naiss,
                              Date date_emb) {

        ProgrammeurBean p = new ProgrammeurBean();
        p.setMatricule(matricule);
        p.setNom(nom);
        p.setPrenom(prenom);
        p.setSecondPrenom(secondPrenom);
        p.setAdresse(adresse);
        p.setPseudo(pseudo);
        p.setResponsable(responsable);
        p.setHobby(hobby);
        p.setDate_naiss(date_naiss);
        p.setDate_emb(date_emb);

        return p;
    }

    public int insertProgrammeur(ActionsBD db, ProgrammeurBean p) {

        String date_naiss_d = p.getDate_naiss().toString().substring(8);
        String date_naiss_m = p.getDate_naiss().toString().substring(5, 7);
        String date_naiss_y = p.getDate_naiss().toString().substring(0, 4);

        String date_emb_d = p.getDate_emb().toString().substring(8);
        String date_emb_m = p.getDate_emb().toString().substring(5, 7);
        String date_emb_y = p.getDate_emb().toString().substring(0, 4);

        return db.ajouterProgrammeur(
                p.getMatricule(),
                p.getNom(),
                p.getPrenom(),
                p.getSecondPrenom(),
                p.getAdresse(),
                p.getPseudo(),
                p.getResponsable(),
                p.getHobby(),
                date_naiss_d,
                date_naiss_m,
                date_naiss_y,
                date_emb_d,
                date_emb_m,
                date_emb_y
        );
    }

    public int modifyProgrammeur(ActionsBD db,
                                 ProgrammeurBean a,
                                 String matricule,
                                 String nom,
                                 String prenom,
                                 String secondPrenom,
                                 String adresse,
                                 String pseudo,
                                 String responsable,
                                 String hobby,
                                 Date date_naiss,
                                 Date date_emb) {
        a.setMatricule(matricule);
        a.setNom(nom);
        a.setPrenom(prenom);
        a.setSecondPrenom(secondPrenom);
        a.setAdresse(adresse);
        a.setPseudo(pseudo);
        a.setResponsable(responsable);
        a.setHobby(hobby);
        a.setDate_naiss(date_naiss);
        a.setDate_emb(date_emb);

        LocalDate local_date_naiss = date_naiss.toLocalDate();
        LocalDate local_date_emb = date_emb.toLocalDate();

        return db.modifierProgrammeur(matricule,
                nom,
                prenom,
                secondPrenom,
                adresse,
                pseudo,
                responsable,
                hobby,
                Integer.toString(local_date_naiss.getDayOfMonth()),
                Integer.toString(local_date_naiss.getMonthValue()),
                Integer.toString(local_date_naiss.getYear()),
                Integer.toString(local_date_emb.getDayOfMonth()),
                Integer.toString(local_date_emb.getMonthValue()),
                Integer.toString(local_date_emb.getYear()));
    }
}
