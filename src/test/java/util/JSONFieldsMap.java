package util;

/***
 * Maps dataset fields to be used in JAVA code.
 */
public final class JSONFieldsMap {
    public static final String MATRICULE = "MATRICULE";
    public static final String NOM = "NOM";
    public static final String PRENOM = "PRENOM";
    public static final String SECOND_PRENOM = "SECOND_PRENOM";
    public static final String PSEUDO = "PSEUDO";
    public static final String RESPONSABLE = "RESPONSABLE";
    public static final String HOBBY = "HOBBY";
    public static final String DATE_NAISS = "DATE_NAISS";
    public static final String DATE_EMB = "DATE_EMB";
    public static final String ADRESSE = "ADRESSE";
    public static final String NAME = "NAME";
    public static final String EXPECTED_TEST_RESULT = "EXPECTED_TEST_RESULT";
}
