package util;

import modele.ProgrammeurBean;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/***
 * Extracts dataset inputs from a JSON file.
 */
public class JSONDatasetHelper {
    private final JSONArray dataset;
    private final List<JSONObject> failures;
    private final List<JSONObject> success;

    public JSONDatasetHelper(String resourceFileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        dataset = (JSONArray) parser.parse(new FileReader(getClass().getResource("/" + resourceFileName).getFile()));
        failures = getDataByExpectedTestResult(ExpectedTestResult.FAILURE);
        success = getDataByExpectedTestResult(ExpectedTestResult.SUCCESS);
    }

    /***
     *
     * @param expected Either failure or success.
     * @return A List of JSONObjects corresponding to the expected test result
     */
    public List<JSONObject> getDataByExpectedTestResult(ExpectedTestResult expected) {

        List<JSONObject> list = new ArrayList<>();
        String searchString = null;
        // CUSTOM FORMAT
        // TODO: JSON schema + validator
        switch (expected) {
            case FAILURE:
                searchString = "FAILURE";
                break;
            case SUCCESS:
                searchString = "SUCCESS";
                break;
        }

        Iterator<JSONObject> iterator = dataset.iterator();
        while (iterator.hasNext()) {
            JSONObject object = iterator.next();
            String objectName = (String) object.get(JSONFieldsMap.EXPECTED_TEST_RESULT);
            if (objectName.contains(searchString))
                list.add(object);
        }

        return list;
    }

    /***
     *
     * @param o JSONObject representing the Programmeur
     * @return Corresponding ProgrammeurBean's object
     */
    public ProgrammeurBean JSONtoProgrammeurBean(JSONObject o) {
        try {
            ProgrammeurBean bean = new ProgrammeurBean();
            bean.setNom(String.valueOf(o.get(JSONFieldsMap.NOM)));
            bean.setMatricule(String.valueOf(o.get(JSONFieldsMap.MATRICULE)));
            bean.setPseudo(String.valueOf(o.get(JSONFieldsMap.PSEUDO)));
            bean.setPrenom(String.valueOf(o.get(JSONFieldsMap.PRENOM)));
            bean.setSecondPrenom(String.valueOf(o.get(JSONFieldsMap.SECOND_PRENOM)));
            bean.setHobby(String.valueOf(o.get(JSONFieldsMap.HOBBY)));
            bean.setResponsable(String.valueOf(o.get(JSONFieldsMap.RESPONSABLE)));
            bean.setAdresse(String.valueOf(o.get(JSONFieldsMap.ADRESSE)));
            bean.setDate_emb(Date.valueOf(String.valueOf(o.get(JSONFieldsMap.DATE_EMB))));
            bean.setDate_naiss(Date.valueOf(String.valueOf(o.get(JSONFieldsMap.DATE_NAISS))));
            return bean;
        } catch (Exception e) {
            System.out.println(o + " not parsable to ProgrammeurBean class");
        }
        return null;
    }

    public List<JSONObject> getTestsCasesAsList(ExpectedTestResult result) {
        if (result == ExpectedTestResult.FAILURE)
            return failures;
        if (result == ExpectedTestResult.SUCCESS)
            return success;
        return null;
    }

    /***
     * Expected test results
     */
    public enum ExpectedTestResult {
        FAILURE,
        SUCCESS
    }
}
