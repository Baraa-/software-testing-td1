# Instructions to follow to test GUI application.

## Programmeur>Afficher>Tous

* Display a read-only text box
* Display relevant data.

## Programmeur>Modifier

* All textboxes enabled
* Placeholders "date" and "année" and selected date for birthdates and hire dates.
* Only "Rechercher" and "Annuler" buttons enabled.
* If matricule doesn't exist, warning box "Programmeur introuvable"
* If the programmer exists, all relevant data will populate the textboxes

## Programmeur>Supprimer

* All textboxes except "Matricule" disabled.
* Only "Valider" & "Annuler" buttons enabled.
* Placeholders "date" and "année" and selected date for birthdates and hire dates.
* In case of a successful deletion, success box "Suppression réussie"
* If the programmer doesn't exist, warning box "Programmeur introuvable"

## Programmeur>Ajouter

* All textboxes enabled
* Placeholders "date" and "année" and selected date for birthdates and hire dates.
* Only "Rechercher" button disabled.
* In case of a successful addition, success box "Ajout réussi"

## Programmeur>Action>Quitter

* In case the user wants to quit, a notification box will popup asking for confirmation :  "Voulez-vous vraiment
  quitter?" 




