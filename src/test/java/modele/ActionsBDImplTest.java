package modele;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.JSONDatasetHelper;
import util.JSONFieldsMap;
import util.ProgrammeurBeanHelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ActionsBDImplTest {
    private static JSONDatasetHelper datasetHelper;
    private static ActionsBDImpl db;
    private static ProgrammeurBeanHelper beanHelper;

    /**
     * Create a new connection with the database using the class ActionsBDImpl
     */
    @BeforeAll
    public static void setUp() throws IOException, ParseException {
        db = new ActionsBDImpl();
        Path path = Paths.get("sql_script.sql");
        StringBuilder sb = new StringBuilder();
        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(s -> sb.append(s).append("\n"));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        String sql = sb.toString();
        String[] queries = sql.split(";");
        for (String query: queries) {
            try {
                db.getDbConn().prepareStatement(query).executeUpdate();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
        datasetHelper = new JSONDatasetHelper("dataset.json");
        beanHelper = new ProgrammeurBeanHelper();
    }

    /**
     * Close the connection that has been open by setUp
     */
    @AfterAll
    public static void tearDown() {
        db.fermerRessources();
    }


    /**
     * Test that the function getResultSet can connect to the databse and return the result of a query
     *
     * @throws Exception
     */
    @Test
    void databaseConnectivityTest() throws Exception {
        ResultSet rs = db.getResultSet("Select True");
        int i = 0;
        boolean isConnected = false;
        while (rs.next()) {
            i++;
            isConnected = rs.getBoolean("True");
        }
        assertEquals(i, 1);
        assertTrue(isConnected);
    }

    /**
     * Verify that fermerRessources close completely Database connection
     */
    @Test
    void databaseFermerRessourcesWorks() {
        db.fermerRessources();
        assertThrows(NullPointerException.class, () -> db.getResultSet("Select True"));
        db = new ActionsBDImpl();
    }

    /**
     * Verify that the program doesn't crash because of a SQL Syntax Exception
     */
    @Test
    void getResultSetCatchSyntaxError() {
        assertDoesNotThrow(() -> db.getResultSet("Select count(*) PROGRAMMEUR"));
    }

    /**
     * Verify that ajouterProgrammeur() add correctly the new programmeur in the Database
     */
    @Test
    void ajouterProgrammeurDoesNotReturnErrorCode() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                assertEquals(0, beanHelper.insertProgrammeur(db, a));
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Verify that modifierProgrammeur() modifie correctly a programmeur that exist in the Database
     */
    @Test
    void modifierProgrammeurDoesNotReturnErrorCode() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                assertEquals(0,
                        beanHelper.modifyProgrammeur(db,
                                a,
                                a.getMatricule(),
                                "ALBAKRI",
                                "Baraa",
                                "bg",
                                "32 Avenue de la République",
                                "baraaaaaaaaa",
                                "Alexandre",
                                "Cuisine",
                                java.sql.Date.valueOf("1988-01-02"),
                                new java.sql.Date(Instant.now().toEpochMilli())));
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Verify that supprimerProgrammeur() delete correctly a programmeur from the Database
     */
    @Test
    void supprimerProgrammeurDoesNotReturnErrorCode() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                assertEquals(0, db.supprimerProgrammeur(a.getMatricule()));
            }
        }
    }

    /**
     * Verify that getProgrammeur() get the programmeur from the Database and return a correct ProgrammeurBean object
     */
    @Test
    void getProgrammeurFromDBReturnCorrectValues() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                ProgrammeurBean b = db.getProgrammeur(a.getMatricule());
                assertEquals(b.toString(), a.toString());
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Verify that modifying a programmeur after adding it work correctly
     */
    @Test
    void modifyProgrammeurAfterAddItWorks() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                ProgrammeurBean b = db.getProgrammeur(a.getMatricule());
                assertEquals(b.toString(), a.toString());
                beanHelper.modifyProgrammeur(db,
                        a,
                        a.getMatricule(),
                        "ALBAKRI",
                        "Baraa",
                        "pas bg",
                        "32 Avenue de la République",
                        "baraaaaaaaaa",
                        "Alexandre",
                        "Cuisine",
                        java.sql.Date.valueOf("1988-01-02"),
                        new java.sql.Date(Instant.now().toEpochMilli()));
                ProgrammeurBean c = db.getProgrammeur(a.getMatricule());
                assertNotEquals(c.toString(), b.toString());
                assertEquals(c.toString(), a.toString());
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Verify that getProgrammeurs return the list of all programmeurs that exist in the Database
     *
     * @throws SQLException
     */
    @Test
    void getProgrammeursReturnsTheListOfAllProgrammeurs() throws SQLException {
        ResultSet rs = db.getResultSet("Select count(*) as c from PROGRAMMEUR");
        int programmeursNumber = 0;
        while (rs.next()) {
            programmeursNumber = rs.getInt("c");
        }
        int programmeursNumberUsingGet = getProgrammeursNumber();
        assertEquals(programmeursNumber, programmeursNumberUsingGet);
    }

    /**
     * Verify that supprimerProgrammeur() decrease the number of programmeurs in the Database
     * and doesn't leave an empty line
     */
    @Test
    void deleteProgrammeurDecreaseProgrammeursNumber() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            int oldProgrammeursNumber = getProgrammeursNumber();
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                int newProgrammeursNumber = getProgrammeursNumber();
                assertEquals(oldProgrammeursNumber + 1, newProgrammeursNumber);
                db.supprimerProgrammeur(a.getMatricule());
                newProgrammeursNumber = getProgrammeursNumber();
                assertEquals(oldProgrammeursNumber, newProgrammeursNumber);
            }
        }
    }

    /**
     * Verify that the program doesn't crash if we are trying to delete the same programmeur 2 times
     */
    @Test
    void deleteNonExistentProgrammeurDoesNotReturnException() {
        List<JSONObject> invalids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.FAILURE);
        for (JSONObject invalid : invalids) {
            String matricule = String.valueOf(invalid.get(JSONFieldsMap.MATRICULE));
            assertDoesNotThrow(() -> db.supprimerProgrammeur(matricule));
        }
    }

    /**
     * Verify that deleting a not existent programmeur doesn't decrease programmeurs number in the Database
     */
    @Test
    void deleteNonExistentProgrammeurDoesNotDecreaseProgrammeurs() {
        List<JSONObject> invalids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.FAILURE);
        for (JSONObject invalid : invalids) {
            int oldProgrammeursNumber = getProgrammeursNumber();
            String matricule = String.valueOf(invalid.get(JSONFieldsMap.MATRICULE));
            db.supprimerProgrammeur(matricule);
            int newProgrammeursNumber = getProgrammeursNumber();
            assertEquals(oldProgrammeursNumber, newProgrammeursNumber);
        }
    }

    /**
     * Modifying non existent programmeur after deleting it doesn't crash the program
     */
    @Test
    void modifyProgrammeurAfterDeletingItDoesNotReturnException() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                db.supprimerProgrammeur(a.getMatricule());
                assertDoesNotThrow(() -> beanHelper.modifyProgrammeur(db,
                        a,
                        a.getMatricule(),
                        "ALBAKRI",
                        "Baraa",
                        "pas ouf",
                        "32 Avenue de la République",
                        "baraaaaaaaaa",
                        "Alexandre",
                        "Cuisine",
                        Date.valueOf("1988-01-01"),
                        new Date(Instant.now().toEpochMilli())));
            }
        }
    }

    /**
     * Modifying non existent programmeur after deleting it doesn't add a new programmeur in the Database
     */
    @Test
    void modifyProgrammeurAfterDeletingItDoesNotIncreaseProgrammeurs() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                int oldProgrammeursNumber = getProgrammeursNumber();
                beanHelper.insertProgrammeur(db, a);
                db.supprimerProgrammeur(a.getMatricule());
                beanHelper.modifyProgrammeur(db,
                        a,
                        a.getMatricule(),
                        "ALBAKRI",
                        "Baraa",
                        "cheum",
                        "32 Avenue de la République",
                        "baraaaaaaaaa",
                        "Alexandre",
                        "Cuisine",
                        Date.valueOf("1988-01-01"),
                        new Date(Instant.now().toEpochMilli()));
                int newProgrammeursNumber = getProgrammeursNumber();
                assertEquals(oldProgrammeursNumber, newProgrammeursNumber);
            }
        }
    }

    /**
     * Adding a new programmeur with the mitricule of an old programmeur that has been deleted doesnt return SQL exception
     */
    @Test
    void addProgrammeurWithDeletedProgrammeursMarticuleDoesNotReturnException() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                db.supprimerProgrammeur(a.getMatricule());
                assertDoesNotThrow(() -> beanHelper.insertProgrammeur(db, a));
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Adding a new programmeur with the matricule of an old programmeur is working correctly and it increase the number
     * of programmeurs in the Database
     */
    @Test
    void addProgrammeurWithDeletedProgrammeursMarticuleIncreaseProgrammeurs() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            int oldProgrammeursNumber = getProgrammeursNumber();
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                db.supprimerProgrammeur(a.getMatricule());
                beanHelper.insertProgrammeur(db, a);
                int newProgrammeursNumber = getProgrammeursNumber();
                assertEquals(oldProgrammeursNumber + 1, newProgrammeursNumber);
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * Verify that the number of programmeurs increase after the add of a new programmeur in the table PROGRAMMEUR
     */
    @Test
    void addProgrammeurIncreasesProgrammeursNumber() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            int oldProgrammeursNumber = getProgrammeursNumber();
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                int newProgrammeursNumber = getProgrammeursNumber();
                assertEquals(oldProgrammeursNumber + 1, newProgrammeursNumber);
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    @Test
    /***
     * Create a new ProgrammeurBean's instance
     * and add the corresponding record inside the PROGRAMMEUR table inside the database.
     * @result A new Programmeur is added into the database (new record in the PROGRAMMEUR table).
     */
    public void actionBDaddNewProgrammeurCorrectly() {
        List<JSONObject> valids = datasetHelper.getDataByExpectedTestResult(JSONDatasetHelper.ExpectedTestResult.SUCCESS);
        for (JSONObject valid : valids) {
            ProgrammeurBean a = datasetHelper.JSONtoProgrammeurBean(valid);
            assertNotNull(a);
            if (a != null) {
                beanHelper.insertProgrammeur(db, a);
                ProgrammeurBean b = db.getProgrammeur(a.getMatricule());
                Assertions.assertNotNull(b);

                if (b != null) {
                    assertEquals(a.toString(), b.toString());
                }
                deleteProgrammeurAfterTest(a);
            }
        }
    }

    /**
     * private function that return the number of programmeurs in the Database
     *
     * @return the number of programmeurs
     */
    private int getProgrammeursNumber() {
        return db.getProgrammeurs().size();
    }

    /**
     * private function that delete a programmeur from the Database
     *
     * @param a
     */
    private void deleteProgrammeurAfterTest(ProgrammeurBean a) {
        db.supprimerProgrammeur(a.getMatricule());
    }
}
