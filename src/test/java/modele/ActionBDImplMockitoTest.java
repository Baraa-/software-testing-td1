package modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import utils.Constantes;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * In this class, we have the list of tests that could be run without a Database using Mockito
 */
public class ActionBDImplMockitoTest {

    private ResultSet resultSet;

    private Connection dbConn;

    private PreparedStatement pstmt;

    private Statement stmt;

    private ActionsBD actionsBD;

    /**
     * Before each test, prepare the mocked and spied objects that could be used by the test
     *
     * @throws SQLException
     */
    @BeforeEach
    public void setUp() throws SQLException {
        actionsBD = Mockito.spy(ActionsBDImpl.class);
        dbConn = Mockito.mock(Connection.class);
        pstmt = Mockito.mock(PreparedStatement.class);
        resultSet = Mockito.mock(ResultSet.class);
        stmt = Mockito.mock(Statement.class);
        Whitebox.setInternalState(actionsBD, "dbConn", dbConn);
        Mockito.when(dbConn.createStatement()).thenReturn(stmt);
    }

    /**
     * Verify that getProgrammeurs() return the list of all the programmeurs that exist in the resultSet
     *
     * @throws SQLException
     */
    @Test
    void getProgrammeursWorksCorrectly() throws SQLException {
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString("MATRICULE")).thenReturn("1");
        Mockito.when(resultSet.getString("PRENOM")).thenReturn("Torvalds");
        Mockito.when(resultSet.getString("SECOND_PRENOM")).thenReturn("linux");
        Mockito.when(resultSet.getString("NOM")).thenReturn("Linus");
        Mockito.when(resultSet.getString("ADRESSE")).thenReturn("2 avenue Linux Git");
        Mockito.when(resultSet.getString("PSEUDO")).thenReturn("linuxroot");
        Mockito.when(resultSet.getString("RESPONSABLE")).thenReturn("Didier Achvar");
        Mockito.when(resultSet.getString("HOBBY")).thenReturn("Salsa");
        Mockito.when(resultSet.getDate("DATE_NAISS")).thenReturn(Date.valueOf("1969-01-12"));
        Mockito.when(resultSet.getDate("DATE_EMB")).thenReturn(Date.valueOf("2170-01-12"));

        Mockito.when(stmt.executeQuery(Constantes.SELECT_ALL)).thenReturn(resultSet);

        assertEquals(1, actionsBD.getProgrammeurs().size());
    }

    /**
     * Verify that getProgrammeur() return the correct information that could be sent by a Database
     *
     * @throws SQLException
     */
    @Test
    void getProgrammeurReturnsCorrectProgrammeur() throws SQLException {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        final String expected = id + " " + lastName + " " + firstName + " " + secondName + " "
                + address + " " + pseudo + " " + manager + " "
                + hobby + " " + birthDate + " " + hireDate + "\n";

        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString("MATRICULE")).thenReturn(id);
        Mockito.when(resultSet.getString("PRENOM")).thenReturn(firstName);
        Mockito.when(resultSet.getString("SECOND_PRENOM")).thenReturn(secondName);
        Mockito.when(resultSet.getString("NOM")).thenReturn(lastName);
        Mockito.when(resultSet.getString("ADRESSE")).thenReturn(address);
        Mockito.when(resultSet.getString("PSEUDO")).thenReturn(pseudo);
        Mockito.when(resultSet.getString("RESPONSABLE")).thenReturn(manager);
        Mockito.when(resultSet.getString("HOBBY")).thenReturn(hobby);
        Mockito.when(resultSet.getDate("DATE_NAISS")).thenReturn(birthDate);
        Mockito.when(resultSet.getDate("DATE_EMB")).thenReturn(hireDate);
        Mockito.when(actionsBD.getResultSet(Constantes.SELECT_ALL)).thenReturn(resultSet);

        ArrayList<ProgrammeurBean> listeProgrammeurs = actionsBD.getProgrammeurs();

        assertEquals(1, listeProgrammeurs.size());
        assertEquals(expected, listeProgrammeurs.get(0).toString());
    }

    /**
     * Verify that supprimerProgrammeur() execute the right delete query on the Database with the sent Matricule
     *
     * @throws SQLException
     */
    @Test
    void deleteProgrammeurSendQueryWithCorrectMatricule() throws SQLException {
        Mockito.when(dbConn.prepareStatement(Constantes.DELETE_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenReturn(1);

        actionsBD.supprimerProgrammeur("1");

        Mockito.verify(pstmt).setString(1, "1");
        Mockito.verify(pstmt).executeUpdate();
    }

    /**
     * Verify that fermerRessources() close Database connection and statements if they aren't null
     *
     * @throws Exception
     */
    @Test
    void closeConnectionIfThereIsConnection() throws Exception {
        Whitebox.setInternalState(actionsBD, "stmt", stmt);
        Whitebox.setInternalState(actionsBD, "pstmt", pstmt);

        actionsBD.fermerRessources();

        Mockito.verify(dbConn).close();
        Mockito.verify(stmt).close();
        Mockito.verify(pstmt).close();
    }

    /**
     * Verify that fermerRessources() doesn't return an exception if there isn't a connection with the Database
     *
     * @throws Exception
     */
    @Test
    void doesNotCloseNullConnection() throws Exception {
        Whitebox.setInternalState(actionsBD, "dbConn", null);

        assertDoesNotThrow(() -> actionsBD.fermerRessources());

        Mockito.verifyNoInteractions(dbConn);
        Mockito.verifyNoInteractions(stmt);
        Mockito.verifyNoInteractions(pstmt);
    }

    /**
     * Verify that getResultSet run the exact query sent in parameter on the Database
     *
     * @throws Exception
     */
    @Test
    void getResultSetRunsSentQuery() throws Exception {
        actionsBD.getResultSet(Constantes.SELECT_ALL);

        Mockito.verify(dbConn).createStatement();
        Mockito.verify(stmt).executeQuery(Constantes.SELECT_ALL);
    }

    /**
     * Verify that afficherProgrammeurs() send the correct content of PROGRAMMEUR table
     *
     * @throws SQLException
     */
    @Test
    void showProgrammeursReturnsRightString() throws SQLException {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        final String expected = id + " " + lastName + " " + firstName + " " + secondName + " "
                + address + " " + pseudo + " " + manager + " "
                + hobby + " " + birthDate + " " + hireDate + "\n";
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString("MATRICULE")).thenReturn(id);
        Mockito.when(resultSet.getString("PRENOM")).thenReturn(firstName);
        Mockito.when(resultSet.getString("SECOND_PRENOM")).thenReturn(secondName);
        Mockito.when(resultSet.getString("NOM")).thenReturn(lastName);
        Mockito.when(resultSet.getString("ADRESSE")).thenReturn(address);
        Mockito.when(resultSet.getString("PSEUDO")).thenReturn(pseudo);
        Mockito.when(resultSet.getString("RESPONSABLE")).thenReturn(manager);
        Mockito.when(resultSet.getString("HOBBY")).thenReturn(hobby);
        Mockito.when(resultSet.getDate("DATE_NAISS")).thenReturn(birthDate);
        Mockito.when(resultSet.getDate("DATE_EMB")).thenReturn(hireDate);
        Mockito.when(actionsBD.getResultSet(Constantes.SELECT_ALL)).thenReturn(resultSet);

        String sentValue = actionsBD.afficherProgrammeurs();

        assertEquals(expected, sentValue);
    }

    /**
     * Verify that getProgrammeur() send the correct query with Matricule to the Database
     *
     * @throws SQLException
     */
    @Test
    void getProgrammeurSendsRightProgrammeur() throws SQLException {
        Mockito.when(dbConn.prepareStatement(Constantes.SELECT_UNIQUE)).thenReturn(pstmt);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        Mockito.when(resultSet.getString("MATRICULE")).thenReturn(id);
        Mockito.when(resultSet.getString("PRENOM")).thenReturn(firstName);
        Mockito.when(resultSet.getString("SECOND_PRENOM")).thenReturn(secondName);
        Mockito.when(resultSet.getString("NOM")).thenReturn(lastName);
        Mockito.when(resultSet.getString("ADRESSE")).thenReturn(address);
        Mockito.when(resultSet.getString("PSEUDO")).thenReturn(pseudo);
        Mockito.when(resultSet.getString("RESPONSABLE")).thenReturn(manager);
        Mockito.when(resultSet.getString("HOBBY")).thenReturn(hobby);
        Mockito.when(resultSet.getDate("DATE_NAISS")).thenReturn(birthDate);
        Mockito.when(resultSet.getDate("DATE_EMB")).thenReturn(hireDate);
        Mockito.when(pstmt.executeQuery()).thenReturn(resultSet);
        final String expected = id + " " + lastName + " " + firstName + " " + secondName + " "
                + address + " " + pseudo + " " + manager + " "
                + hobby + " " + birthDate + " " + hireDate + "\n";

        ProgrammeurBean sentprog = actionsBD.getProgrammeur("1");

        assertEquals(expected, sentprog.toString());
        Mockito.verify(pstmt).setString(1, "1");
        Mockito.verify(pstmt).executeQuery();
    }

    /**
     * Verify that modifierProgrammeur() send the correct query with the correct params to the Database
     *
     * @throws SQLException
     */
    @Test
    void modifyProgrammeurSendsCorrectValues() throws SQLException {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        Mockito.when(dbConn.prepareStatement(Constantes.UPDATE_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenReturn(1);

        LocalDate localBirthDate = birthDate.toLocalDate();
        LocalDate localHireDate = hireDate.toLocalDate();

        actionsBD.modifierProgrammeur(id,
                lastName,
                firstName,
                secondName,
                address,
                pseudo,
                manager,
                hobby,
                Integer.toString(localBirthDate.getDayOfMonth()),
                Integer.toString(localBirthDate.getMonthValue()),
                Integer.toString(localBirthDate.getYear()),
                Integer.toString(localHireDate.getDayOfMonth()),
                Integer.toString(localHireDate.getMonthValue()),
                Integer.toString(localHireDate.getYear()));

        String expectedBirthDate = dateToString(localBirthDate);
        String expectedHireDate = dateToString(localHireDate);

        Mockito.verify(pstmt).setString(1, lastName);
        Mockito.verify(pstmt).setString(2, firstName);
        Mockito.verify(pstmt).setString(3, secondName);
        Mockito.verify(pstmt).setString(4, address);
        Mockito.verify(pstmt).setString(5, pseudo);
        Mockito.verify(pstmt).setString(6, manager);
        Mockito.verify(pstmt).setString(7, hobby);
        Mockito.verify(pstmt).setString(8, expectedBirthDate);
        Mockito.verify(pstmt).setString(9, expectedHireDate);
        Mockito.verify(pstmt).setString(10, id);
        Mockito.verify(pstmt).executeUpdate();
    }

    /**
     * Verify that ajouterProgrammeur send the correct insert query with the correct params to the Database
     *
     * @throws Exception
     */
    @Test
    void addProgrammeurSendsCorrectValues() throws Exception {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        Mockito.when(dbConn.prepareStatement(Constantes.INSERT_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenReturn(1);

        LocalDate localBirthDate = birthDate.toLocalDate();
        LocalDate localHireDate = hireDate.toLocalDate();

        actionsBD.ajouterProgrammeur(id,
                lastName,
                firstName,
                secondName,
                address,
                pseudo,
                manager,
                hobby,
                Integer.toString(localBirthDate.getDayOfMonth()),
                Integer.toString(localBirthDate.getMonthValue()),
                Integer.toString(localBirthDate.getYear()),
                Integer.toString(localHireDate.getDayOfMonth()),
                Integer.toString(localHireDate.getMonthValue()),
                Integer.toString(localHireDate.getYear()));

        String expectedBirthDate = dateToString(localBirthDate);
        String expectedHireDate = dateToString(localHireDate);
        Mockito.verify(pstmt).setString(1, id);
        Mockito.verify(pstmt).setString(2, lastName);
        Mockito.verify(pstmt).setString(3, firstName);
        Mockito.verify(pstmt).setString(4, secondName);
        Mockito.verify(pstmt).setString(5, address);
        Mockito.verify(pstmt).setString(6, pseudo);
        Mockito.verify(pstmt).setString(7, manager);
        Mockito.verify(pstmt).setString(8, hobby);
        Mockito.verify(pstmt).setString(9, expectedBirthDate);
        Mockito.verify(pstmt).setString(10, expectedHireDate);
        Mockito.verify(pstmt).executeUpdate();
    }

    /**
     * Verify that the program doesn't crash if the Database send "0" (which mean an error) after the execution
     * of insert query on the Database
     *
     * @throws SQLException
     */
    @Test
    void programmeDoesNotCrashIfInsertIsKO() throws SQLException {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        Mockito.when(dbConn.prepareStatement(Constantes.INSERT_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenThrow(new SQLException());

        LocalDate localBirthDate = birthDate.toLocalDate();
        LocalDate localHireDate = hireDate.toLocalDate();

        assertDoesNotThrow(() -> actionsBD.ajouterProgrammeur(id,
                lastName,
                firstName,
                secondName,
                address,
                pseudo,
                manager,
                hobby,
                Integer.toString(localBirthDate.getDayOfMonth()),
                Integer.toString(localBirthDate.getMonthValue()),
                Integer.toString(localBirthDate.getYear()),
                Integer.toString(localHireDate.getDayOfMonth()),
                Integer.toString(localHireDate.getMonthValue()),
                Integer.toString(localHireDate.getYear())));
    }

    /**
     * Verify that the program doesn't crash if the Database send "0" (which mean an error) after the execution
     * of update query on the Database
     *
     * @throws SQLException
     */
    @Test
    void programmeDoesNotCrashIfUpdateIsKO() throws SQLException {
        final String id = "1";
        final String lastName = "Torvalds";
        final String firstName = "Linus";
        final String secondName = "linux";
        final String address = "2 avenue Linux Git";
        final String pseudo = "linuxroot";
        final String manager = "Didier Achvar";
        final String hobby = "Salsa";
        final Date birthDate = Date.valueOf("1969-01-12");
        final Date hireDate = Date.valueOf("2170-01-12");
        Mockito.when(dbConn.prepareStatement(Constantes.UPDATE_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenThrow(new SQLException());

        LocalDate localBirthDate = birthDate.toLocalDate();
        LocalDate localHireDate = hireDate.toLocalDate();

        assertDoesNotThrow(() -> actionsBD.modifierProgrammeur(id,
                lastName,
                firstName,
                secondName,
                address,
                pseudo,
                manager,
                hobby,
                Integer.toString(localBirthDate.getDayOfMonth()),
                Integer.toString(localBirthDate.getMonthValue()),
                Integer.toString(localBirthDate.getYear()),
                Integer.toString(localHireDate.getDayOfMonth()),
                Integer.toString(localHireDate.getMonthValue()),
                Integer.toString(localHireDate.getYear())));
    }

    /**
     * Verify that the program doesn't crash if the Database send "0" (which mean an error) after the execution
     * of delete query on the Database
     *
     * @throws SQLException
     */
    @Test
    void programmeDoesNotCrashIfDeleteIsKO() throws SQLException {
        Mockito.when(dbConn.prepareStatement(Constantes.DELETE_UNIQUE)).thenReturn(pstmt);
        Mockito.when(pstmt.executeUpdate()).thenThrow(new SQLException());

        assertDoesNotThrow(() -> actionsBD.supprimerProgrammeur("1"));
    }

    /**
     * Verify that the program doesn't crash if the connection of the Database doesn't generate a correct statement
     *
     * @throws SQLException
     */
    @Test
    void programmeDoesNotCrashIfStatementIsNull() throws SQLException {
        Mockito.when(dbConn.prepareStatement(Mockito.anyString())).thenReturn(null);

        assertDoesNotThrow(() -> actionsBD.getResultSet("select True"));
    }

    private String dateToString(LocalDate localDate) {
        return localDate.getYear() + "-" + localDate.getMonthValue() + "-" + localDate.getDayOfMonth();
    }
}
