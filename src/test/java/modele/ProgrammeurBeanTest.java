package modele;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.ProgrammeurBeanHelper;

import java.sql.Date;
import java.time.Instant;

class ProgrammeurBeanTest {
    private static ProgrammeurBeanHelper beanHelper;

    @BeforeAll
    static void setUp() {
        beanHelper = new ProgrammeurBeanHelper();
    }

    @Test
    void testToString() {
        final String id = "88";
        final String lastName = "KISSOUM";
        final String secondName = "bg";
        final String firstName = "Fares";
        final String address = "30 Avenue de la République";
        final String pseudo = "faresSansAccent";
        final String manager = "J2A";
        final String hobby = "Boxe";
        final Date birthDate = Date.valueOf("1998-01-01");
        final Date hireDate = new Date(Instant.now().toEpochMilli());

        final String expected = id + " " + lastName + " " + firstName + " " + secondName + " "
                + address + " " + pseudo + " " + manager + " "
                + hobby + " " + birthDate + " " + hireDate + "\n";

        ProgrammeurBean a = beanHelper.of(
                id,
                lastName,
                firstName,
                secondName,
                address,
                pseudo,
                manager,
                hobby,
                birthDate,
                hireDate
        );

        Assertions.assertEquals(expected, a.toString());
    }
}
