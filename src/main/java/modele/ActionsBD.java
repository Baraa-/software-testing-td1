package modele;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Interface contenant les déclaratios des méthodes d'échanges avec la BDD
 */
public interface ActionsBD {
    ResultSet getResultSet(String req);

    ArrayList getProgrammeurs();

    ProgrammeurBean getProgrammeur(String matricule);

    String afficherProgrammeurs();

    int modifierProgrammeur(String matricule, String nom, String prenom, String secondPrenom, String adresse, String pseudo, String responsable, String hobby, String Jdate_naiss, String Mdate_naiss, String Adate_naiss, String Jdate_emb, String Mdate_emb, String Adate_emb);

    int supprimerProgrammeur(String matricule);

    int ajouterProgrammeur(String matricule, String nom, String prenom, String secondPrenom, String adresse, String pseudo, String responsable, String hobby, String Jdate_naiss, String Mdate_naiss, String Adate_naiss, String Jdate_emb, String Mdate_emb, String Adate_emb);

    void fermerRessources();
}